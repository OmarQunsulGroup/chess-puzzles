#!/usr/bin/python
import chess
import glob
import json
from collections import defaultdict
import sys
import random
import chess.engine
import time
import requests

r = requests.get('https://chessduo.com/api/random_games')
response = r.json()

games = response["finished_games"]

for game_data in random.sample(games, 10):
    # Query
    san_moves = list(map(lambda move: move["san"], game_data["moves"]))

    # Initialize the Chess Board
    board = chess.Board()

    # Initialize Stockfish Engine
    engine = chess.engine.SimpleEngine.popen_uci("/usr/bin/stockfish")

    made_moves = []

    for san_move in san_moves[0:]:
        made_moves.append(san_move)
        try:
            board.push_san(san_move)
        except:
            engine.quit()
            print("ERROR")
            exit(0)

        info = engine.analyse(board, chess.engine.Limit(time=0.1))

        # Analyze
        score = info["score"]
        mate_score = score.is_mate()
        if score.is_mate() and abs(score.white().mate()) > 0:
            score = score.white().mate()
            if abs(score) < 4:
                print(" ".join(made_moves))
                if score > 0:
                    print(f"White to checkmate in %d" % abs(score))
                    winner_side = "W"
                else:
                    print(f"Black to checkmate in %d" % abs(score))
                    winner_side = "B"

                fen = board.fen()
                print(fen)

                fen_side = fen.split(" ")[1]
                if fen_side == "w" and winner_side == "W":
                    pass
                elif fen_side == "b" and winner_side == "B":
                    pass
                else:
                    print("SIDE ERROR")
                    engine.quit()
                    exit(1)

                # Calculating the answer
                answer = []
                while not board.is_checkmate():
                    result = engine.play(board, chess.engine.Limit(time=0.4))
                    move = result.move
                    san = board.san(move)
                    answer.append(san)
                    board.push(move)
                    time.sleep(0.2)

                data = {
                  "fen": fen,
                  "answer": answer,
                  "moves": abs(score),
                  "side": winner_side
                }

                print(data)
                r = requests.post('https://chessduo.com/en/puzzles', json=data)
                print("Answer: ")
                response = r.json()
                print(response)

                # Saving Puzzle Locally

                #json_path = f'puzzles/{game_data["game_id"]}.json'
                #data["game_id"] = game_data["game_id"]
                #data["puzzle_id"] = response["id"]
                #data["puzzle_link"] = response["link"]
                #f = open(json_path, 'w')
                #f.write(json.dumps(data))
                #f.close()
                #print("JSON file saved:")
                #print(json_path)
            break

    engine.quit()
