#!/usr/bin/python
import chess
import chess.svg
import json
import glob
import random
import subprocess
import twitter
import os
import requests

api = twitter.Api(consumer_key=os.environ.get('CONSUMER_KEY'),
                  consumer_secret=os.environ.get('CONSUMER_SECRET'),
                  access_token_key=os.environ.get('ACCESS_TOKEN_KEY'),
                  access_token_secret=os.environ.get('ACCESS_TOKEN_SECRET'))

# Puzzle from Chessduo
puzzle = requests.get('https://chessduo.com/api/random_puzzle').json()
puzzle["moves"] = puzzle["moves"] if "moves" in puzzle else len(puzzle["answer"]) // 2 + 1
puzzle["puzzle_link"] = puzzle["puzzle_link"] if "puzzle_link" in puzzle else f'https://www.chessduo.com/en/puzzles/{puzzle["id"]}'
print(puzzle)

# Think of converting them into temp files instead
svg_path = 'temp.svg'
png_path = 'temp.png'
svg = chess.svg.board(chess.Board(puzzle["fen"]), size=350, orientation= chess.WHITE if puzzle["side"] == "W" else chess.BLACK)
svg_file = open(svg_path, 'w')
svg_file.write(svg)
svg_file.close()
rc = subprocess.call(f'convert -density 1200 -resize 450x450 {svg_path} {png_path}'.split(" "))
os.remove(svg_path)

moves = puzzle["moves"]

side = "White" if puzzle["side"] == "W" else "Black"

puzzle_url = puzzle["puzzle_link"]

if moves == 1:
    sentence = f'Here is a quick chess puzzle for you. {side} to win in only 1 move. Try to solve it here {puzzle_url}. #Chess #Puzzle.'
else:
    if random.random() > 0.7:
        sentence = f'Can you solve this Chess Puzzle? {side} to win in only {moves} moves. Solve it here {puzzle_url}. #Chess #Puzzle'
    else:
        if random.random() > 0.5:
            sentence = f'Here is another interesting Chess puzzle for you. {side} to win in only {moves} moves. Solve it now on {puzzle_url}. #Chess #Puzzle #Thinking'
        else:
            sentence = f'If you are bored, here is another daily Chess Puzzle for you. {side} to win in only {moves} moves. Solve it now on {puzzle_url}. #Chess #Puzzle #Thinking'

file = open(png_path, "rb")
status = api.PostUpdate(sentence, media=file)
os.remove(png_path)
