FROM python:3.10-buster

RUN wget https://stockfishchess.org/files/stockfish_14.1_linux_x64_avx2.zip

RUN unzip stockfish_14.1_linux_x64_avx2.zip
RUN rm stockfish_14.1_linux_x64_avx2.zip
RUN mv stockfish_14.1_linux_x64_avx2/stockfish_14.1_linux_x64_avx2 /usr/bin/stockfish
RUN chmod +x /usr/bin/stockfish

RUN apt-get update --fix-missing -qq
RUN apt-get -y install python3 python3-pip imagemagick
ADD pyproject.toml /
ADD poetry.lock /
RUN ["pip3", "install", "poetry"]
RUN ["poetry", "install"]

ADD tweet.py /
